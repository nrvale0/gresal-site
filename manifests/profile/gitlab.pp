class site::profile::gitlab(
  $users = { 'admin' => { 'password' => '5iveL!fe', 'is_admin' => true, } }
) inherits ::site::profile::gitlab::params {
  require ::site::profile::pe::agent
  require ::mysql::client, ::mysql::server
  require ::gitlab

  validate_hash($users)
#  create_resources('site::profile::gitlab::user', $users)
}
