class site::profile::pe::master(
) inherits ::site::profile::pe::master::params {
  require ::site::profile::base
  require ::site::profile::pe

  file { '/etc/puppetlabs/puppet/hiera.yaml':
    ensure => file,
    owner => 'root', group => 'root', mode => '0755',
    source => "puppet:///modules/${module_name}/profile/pe/master/hiera.yaml",
    notify => Service['pe-httpd'],
  }

  if !defined_with_params(Service['pe-httpd'], { ensure => running, }) {
    service { 'pe-httpd': ensure => running, enable => true, }
  } 
}
