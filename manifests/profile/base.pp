class site::profile::base inherits site::profile::base::params {

  include ::ntp

  @@host { $::hostname:
    ensure => present,
    ip => $::virtual ? { 
      'virtualbox' => $::ipaddress_eth1,
      default => $::ipaddress_eth0,
    },  
    host_aliases => $hostname,
  }

  host { 'localhost':
    ensure => present,
    ip => '127.0.0.1',
    host_aliases => 'localhost.localdomain',
  }

  Host <<||>>
}
