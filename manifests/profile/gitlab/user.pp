define site::profile::gitlab::user(
  $user = $name, $password, $is_admin = false,
) {

  validate_string($account)
  validate_string($password)
  validate_bool($is_admin)

  # get the admin account API private-token
  web_request { "get the GitLab ${user} private token":
    post => 'https://localhost/session',
    returns => '200',
    parameters => { 'login' => "${user}", 'password' => "${password}" },
    loglevel => debug,
    log_to => '/tmp/REST',
  }

#  web_request { "create GitLab user ${user}":
#
#  } -> web_request { "set GitLab user ${user} password":
#
#  }
}
