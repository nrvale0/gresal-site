class site::profile::base::params {
  case $::osfamily {
    'redhat', 'debian': {}
    default: {fail("OS family ${::osfamily} not supported!")}
  }
}
